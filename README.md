## 插件使用方法：
	
 `<color-picker-lay @confirm="confirm" :colorList="colorList"></color-picker-lay>`
  
## 配置参数：
 
 属性名|类型|默认值|说明
 :--:|:--:|:--:|-
 colorList|Array|一大堆颜色值|自定义颜色会覆盖默认 格式:["颜色","颜色","颜色",...]

## 事件：

 事件名|说明|返回值
 :--:|:--:|-
 @confirm|点击确认或者推荐颜色组触发的事件|颜色值
 
## 说明：
 
 此插件依赖scss，请务必安装！！！

 
## 示例：
 
 ```
	 <template>
	 	<view class="content">
	 		<input type="text" :style="{'background':bg}">
	 		<color-picker-lay @confirm="confirm" :colorList="colorList"></color-picker-lay>
	 	</view>
	 </template>
	 
	 <script>
	 	export default {
	 		data() {
	 			return {
	 				bg: "",
	 				colorList: [
	 					'#000000', '#222222'
	 				]
	 			}
	 		},
	 		onLoad() {
	 
	 		},
	 		methods: {
	 			confirm(e) {
	 				this.bg = e;
	 				console.log(e)
	 			}
	 		}
	 	}
	 </script>
	 
	 <style lang="scss">
	 	.content {
	 		padding: 20vw;
	 		background: #666;
	 
	 		input {
	 			width: 150px;
	 			border: 2px solid #fff;
	 			padding: 20px;
	 			margin-bottom: 20px;
	 		}
	 	}
	 </style>
 
 ```
 